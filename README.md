# gitlab-docker-flow

Simple exploration into building docker pipeline with Gitlab CI

Create a Docker pipeline using GItlab CI that will:
* Create Base docker image
* Test / Lint Base docker image
* Create production docker image (Remove /test and other waste files, prune dependancies)
* Add images to Gitlab Container registry

### Sample Pipeline
![alt text](/img/pipeline.png)

### Sample Images in Gitlab Container Registry
![alt text](/img/CR.png)

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes.

### Prerequisites

- Docker
- Node

### Installing

Using Node

Install: `npm i`

Lint: `standard`

Test: `npm run test`

Start: `node .`


### Runnning Locally

#### Node
```
node .
```

#### Docker
Create / Build the Docker image
```
docker build -t <project-name>/node-web-app .
```

Run the image 
```
docker run -p 3300:8080 -d <project-name>/node-web-app
```


## Running the tests

Node

```
npm run test
```
Docker

Test the code inside the image by overiding the default CMD in the dockerfile. 

```
docker run <project-name>/node-web-app npm test
```

## Built With

* [Docker](https://www.docker.com/)
* [Gitlab CI](https://docs.gitlab.com/ee/ci/docker/using_docker_build.html)
* [nodejs](https://nodejs.org/en/)

## Authors

* **Jonathan Goulding**

## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details

## Acknowledgments


https://docs.gitlab.com/ee/ci/docker/using_docker_build.html